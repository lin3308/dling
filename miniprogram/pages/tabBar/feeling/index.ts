// pages/tabBar/feeling/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[
      {
        id:'1',
        title:'文章',
        content:'描述1',
        imagePath:'https://alifei01.cfp.cn/creative/vcg/800/new/VCG211408367959.jpg',
      },
      {
        id:'2',
        title:'文章2',
        content:'描述1',
        imagePath:'https://alifei01.cfp.cn/creative/vcg/800/new/VCG211408367959.jpg',
      },
    ]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
 
    clickMe(e:any){
      console.log(e);
      wx.navigateTo({
        url: '/pages/tabBar/essay/index?id='+e.currentTarget.dataset.id
      })
  }
})