// pages/tabBar/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgList:[
      {
        id:'1',
        imagePath:'https://alifei01.cfp.cn/creative/vcg/800/new/VCG211408367959.jpg',
        location:'山东滕州',
        hukou:'山东滕州',
        birth:'89.12',
        marital:'未婚',
        height:'180cm',
        weight:'70kg',
        education:'本科',
        ooccupation:'教师',
        interest:'读书写作 旅游 美食',
        requirements:'彼此能够相互理解，合得来，自己物质条件一般，所以对于物质没有什么特别要求，希望遇到脾气性格合得来的，如果合适的话，女孩愿意和我在一起，可以考虑去女方那，一起建设美好家园，以在一起和结婚为目的，希望真诚真心来交往。加微信saztc-zzw联系',
      },
      {
        id:'2',
        imagePath:'https://alifei01.cfp.cn/creative/vcg/800/new/VCG211408367959.jpg',
        location:'山东滕州2',
        hukou:'山东滕州',
        birth:'89.12',
        marital:'未婚',
        height:'180cm',
        weight:'70kg',
        education:'本科',
        ooccupation:'教师',
        interest:'读书写作 旅游 美食',
        requirements:'彼此能够相互理解，合得来，自己物质条件一般，所以对于物质没有什么特别要求，希望遇到脾气性格合得来的，如果合适的话，女孩愿意和我在一起，可以考虑去女方那，一起建设美好家园，以在一起和结婚为目的，希望真诚真心来交往。加微信saztc-zzw联系',
      },
      {
        id:'3',
        imagePath:'https://alifei01.cfp.cn/creative/vcg/800/new/VCG211408367959.jpg',
        location:'山东滕州',
        hukou:'山东滕州',
        birth:'89.12',
        marital:'未婚',
        height:'180cm',
        weight:'70kg',
        education:'本科',
        ooccupation:'教师',
        interest:'读书写作 旅游 美食',
        requirements:'彼此能够相互理解，合得来，自己物质条件一般，所以对于物质没有什么特别要求，希望遇到脾气性格合得来的，如果合适的话，女孩愿意和我在一起，可以考虑去女方那，一起建设美好家园，以在一起和结婚为目的，希望真诚真心来交往。加微信saztc-zzw联系',
      },
      {
        id:'4',
        imagePath:'https://alifei01.cfp.cn/creative/vcg/800/new/VCG211408367959.jpg',
        location:'山东滕州',
        hukou:'山东滕州',
        birth:'89.12',
        marital:'未婚',
        height:'180cm',
        weight:'70kg',
        education:'本科',
        ooccupation:'教师',
        interest:'读书写作 旅游 美食',
        requirements:'彼此能够相互理解，合得来，自己物质条件一般，所以对于物质没有什么特别要求，希望遇到脾气性格合得来的，如果合适的话，女孩愿意和我在一起，可以考虑去女方那，一起建设美好家园，以在一起和结婚为目的，希望真诚真心来交往。加微信saztc-zzw联系',
      },
      {
        id:'5',
        imagePath:'https://alifei01.cfp.cn/creative/vcg/800/new/VCG211408367959.jpg',
        location:'山东滕州',
        hukou:'山东滕州',
        birth:'89.12',
        marital:'未婚',
        height:'180cm',
        weight:'70kg',
        education:'本科',
        ooccupation:'教师',
        interest:'读书写作 旅游 美食',
        requirements:'彼此能够相互理解，合得来，自己物质条件一般，所以对于物质没有什么特别要求，希望遇到脾气性格合得来的，如果合适的话，女孩愿意和我在一起，可以考虑去女方那，一起建设美好家园，以在一起和结婚为目的，希望真诚真心来交往。加微信saztc-zzw联系',
      }
    ], 
    page:1,   // 第一页
	  list:[],  // 接受列表数据
    flag:true,  // 防抖开关 防止用户不停的下拉
    scrollHeight: 0
  },

  // 滚动到底触发的方法
lower(){
 console.log("触发",this.data.list)
  if(this.data.flag){
    this.setData({
      flag:false
    })
    //this.getData(); // 疯狂的请求的方法
  }
},
getDate(){
  wx.request({
    url:'https://域名ID/index.php', //必填，其他的都可以不填
    data:{  
      page:this.data.page,
      limit:'10'   // 假设每页10条数据 
    },
    header:{  
       'content-type':'application/json'
    },
    method:'GET',  
    success:(result)=>{
      let imgList=this.data.imgList;
      let page=this.data.page+1;
      imgList=imgList.concat(result.data.imgList);// 拼接回来的数据
      this.setData({
        imgList:imgList,
        page:page
      })
      // 当回来的数据小于十条得时候 不让再请求了 否则继续请求
      if(result.data.list.length<10){
        this.setData({
          flag:false
        })
      }else{
        this.setData({
          flag:true
        })
      }
    },
    fail(){  
        console.log('fail')
    },
    complete(){   
         console.log('complete')   
    }
  })
},
clickMe(e:any){
  console.log(e);
  wx.navigateTo({
    url: '/pages/tabBar/detail/index?id='+e.currentTarget.dataset.id
    })
},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    let scrollHeight = wx.getSystemInfoSync().windowHeight -60;
    this.setData({
    scrollHeight: scrollHeight
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})