// pages/tabBar/my/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
      fileList: [], //图片存放的数组
       arr:[],
        newArr:[] ,  //点击删除按钮的时候 进行赋值
        location:'',
        hukou:'',
        birth:'',
        marital:'',
        height:'',
        weight:'',
        education:'',
        ooccupation:'',
        interest:'',
        requirements:'',
  },
   //点击删除图片
   del(event:any){
      let id = event.detail.index   //能获取到对应的下标
      let newArr = this.data.newArr   //这里引用 上传的数组 
      let fileList = this.data.fileList  //这里是前端页面展示的数组
      newArr.splice(id,1)     //根据下标来删除对应的图片
      fileList.splice(id,1)
      this.setData({
        fileList:fileList,  //在这里进行重新赋值  删除后 图片剩几张就相当于给后台传几张
        newArr:newArr
      })
   },
 //上传访问后台接口
   uoload(event:any){
     var img = event.detail.file;
     let that = this;
     let arr = this.data.newArr
     that.setData({
       fileList:img
      })
     img.map(function(v:any,k:any){  //这里是多文件上传 使用map
       let imgs = v.url;
        //let token = wx.getStorageSync('token');
  
         wx.uploadFile({   //这里一定要用  wx.uploadFile 否则无法穿到后台
         filePath: imgs, //你要上传的路径
         name: 'file',   //你上传到后台的name值
         formData:{    // 如果你要验证你的token 可以用formData来给后台传值
          token:token
         },
         url: '你要上传的接口',
         success(res){
           let img = res.data
           console.log(img)
           arr.push(img);   //返回图片的路径  并追加到新数组里面
           that.setData({
             newArr:arr   //在这里重新赋值，用来做删除
           })
         }
       })
     })
   },

// 提交
handleBtn(){
  wx.request({
    url:"url",
    method:"POST",
    data:{
       hukou:"value",
       location:"value",
       birth:'',
       marital:'',
       height:'',
       weight:'',
       education:''
      
    }
});

},



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})